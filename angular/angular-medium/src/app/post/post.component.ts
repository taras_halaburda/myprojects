import { AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, ChangeDetectionStrategy, Component, ContentChild, DoCheck, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Post } from '../app.component';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
  // changeDetection: ChangeDetectionStrategy.Default /* is started by any user action or change in the condition of a component */
  /* OnPush -  Angular in PostComponent will respond only to the input properties  */
  changeDetection: ChangeDetectionStrategy.OnPush, /* runs when the OnChanges component lifecycle stage is called and when the value of the Observable objects used in the template changes. */
  encapsulation: ViewEncapsulation.None /* ViewEncapsulation.None - cancel encapsulation of styles in the component */
})
export class PostComponent implements 
  OnInit, 
  OnChanges, 
  DoCheck, 
  AfterContentInit, 
  AfterContentChecked, 
  AfterViewInit, 
  AfterViewChecked, 
  OnDestroy  {

  // this variable will be received from outside via the decorator @Input()
  @Input() post: Post;
  // pass through the id number of our post that we want to delete
  @Output() onRemove = new EventEmitter<number>();
  @ContentChild('info', {static: true}) infoRef: ElementRef;

  removePost() {
    this.onRemove.emit(this.post.id);
  }


  // : void - do not return anything from the method
  ngOnChanges(changes: SimpleChanges): void {
    console.log('ngOnChanges', changes);
    
  }

  ngOnInit(): void {
    console.log('ngOnInit');
    
    // console.log(this.infoRef.nativeElement); //  get access to the content contained in app.component.html -> div #info > small
    
  }
  ngDoCheck(): void {
    console.log('ngDoCheck');
    
  }
  ngAfterContentInit(): void {
    console.log('ngAfterContentInit');
    
  }
  ngAfterContentChecked(): void {
    console.log('ngAfterContentChecked');
    
  }
  ngAfterViewInit(): void {
    console.log('ngAfterViewInit');
    
  }
  ngAfterViewChecked(): void {
    console.log('ngAfterViewChecked');
    
  }
  ngOnDestroy(): void {
    console.log('ngOnDestroy');
    
  }

}
