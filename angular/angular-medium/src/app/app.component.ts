import { Component, OnInit } from '@angular/core';

// create an interface and describe the types of future objects
export interface Post {
  title: string;
  text: string;
  id?: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  posts: Post[] = [
    {title: 'Study components', text: 'in the process', id: 1},
    // {title: 'Instruction', text: 'Some text..', id: 2} // comment for lifecycle
  ]
  ngOnInit(): void {
    setTimeout(() => {
      console.log('Our Timeout ***');
      // this.posts[0].title = 'Сhanged title ####' /* working with only changeDetection: ChangeDetectionStrategy.Default */
      this.posts[0] = {
        title: 'ChangeDetectionStrategy.OnPush',/* working with ChangeDetectionStrategy.OnPush */
        text: 'The link of the input parameter has changed',
        id: 3
      }
    }, 5000);
  }
  updatePosts(post: Post) {
    this.posts.unshift(post)
    // console.log('Post', post);
    
  }
  removePost(id: number) {
    console.log('Id to remove', id);
    this.posts = this.posts.filter(p => p.id !== id)
    
  }
}
