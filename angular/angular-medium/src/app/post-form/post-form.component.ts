import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Post } from '../app.component';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.scss']
})
export class PostFormComponent implements OnInit {

  

  @Output() onAdd: EventEmitter<Post> = new EventEmitter<Post>()
  // the first parameter passes a reference to an element in the template, the second parameter is an object
  // static: false - if we transfer parameter in ngOnInit(), we put true in other cases we put static: false
  // requirement of Angular version 8 
  @ViewChild('titleInput', {static: false}) inputRef: ElementRef

  title = ''
  text = ''
  constructor() { }

  ngOnInit(): void {
  }
  addPost() {
    // mini validation via trim()
    if (this.title.trim() && this.text.trim()) {
      const post: Post = {
        title: this.title,
        text: this.text
      }
      // call the method to send data up from PostFormComponent to AppComponent
      this.onAdd.emit(post);
      console.log('New Post:', post);
      // reset text fields in our post to enter new text
      this.title = this.text = ''
      
    }
  }
  focusTitle() {
    console.log(this.inputRef);
    // nativeElement - common DOM element in which we can call all necessary methods
    this.inputRef.nativeElement.focus();
    
  }
}
  