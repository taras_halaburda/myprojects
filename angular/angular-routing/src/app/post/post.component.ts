import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Params, Router } from '@angular/router'
import { Post, } from '../posts.service'; // delete PostsService /* optimised, because the resolver now works */

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  post: Post;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    // private postService: PostsService /* optimised, because the resolver now works */

  ) { }
  ngOnInit(): void {
    /* snapshot - is static object and not transmitting data dynamically  */
    this.post = this.route.snapshot.data.post

    /*  */
    this.route.data.subscribe(data => {
      this.post = data.post
    })

    // this.route.params.subscribe((params: Params) => {
    //   console.log('** Params:', params); // ** Params: {id: "11"} - where "11" is string and therefore the string must be converted to a number via "+"
    //   this.post = this.postService.getById(+params.id);
    // })

    /* optimised, because the resolver now works */
  }
  loadPost() {
    // dynamic loading of elements on the corresponding id from other posts
    this.router.navigate(['/posts', 44])
  }

}
