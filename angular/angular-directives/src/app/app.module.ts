import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';  // (1)
import { StyleDirective } from './directives/style.directive'; // (2)
import { InfoDirective } from './directives/ifnot.directive'; // (3)


@NgModule({
  declarations: [
    AppComponent,
    StyleDirective, // (2) (import StyleDirective from ./directives/style.directive)
    InfoDirective  // (3) (import InfoDirective from './directives/info.directive)
  ],
  imports: [
    BrowserModule,
    FormsModule // import FormsModule // (1)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
