import {Directive, ElementRef, HostBinding, HostListener, Input, Renderer2} from '@angular/core'


// converting a class into a directive
@Directive({
    /*in order to turn this ( selector: 'appStyle')  into a real directive with the possibility of using it as an html attribute, you need to turn it into square brackets.  */
    selector: '[appStyle]' 
}) 

// export 
export class StyleDirective {

  // create decorator @Input() for appStyle="red" in app.component and enter in the variable color
  @Input('appStyle') color: string = 'blue';

  @Input() fontWeight: string = 'normal';

  // prescribe the interface for parameters
  @Input() dirStyles: {border?: string, fontWeight?: string, borderRadius?: string}

  /*  HostBinding  */
  @Input() backgroundColor: string;
  @HostBinding('style.backgroundColor') elBackColor = null; 


 // implemented by the constructor and inject the object el: with type ElementRef
 // inject in Directive another object r: with type Renderer2 for the best performance
 constructor (private el: ElementRef, private r: Renderer2) {
    console.log(el);
    // el.nativeElement.style.color = 'tomato'; /* not optimized option for Angular  */
    
    /* the best option for performance */
    // first parameter is a native element; second parameter - the name of the style we want to change; third - to what value we want to change
    this.r.setStyle(el.nativeElement, 'color', 'gold');
 }
    /*  dynamics in the directive via @HostListener */
    // first parameter - event name in the element where the directive was added; second - ['$event.target']
    // then you need to add a decorator (@HostListener) to the method onClick()
    @HostListener('click', ['$event.target']) onClick(event: Event) {
        console.log(event);
    }
     /* for change background in element from app.component.html */
    @HostListener('mouseenter') onEnter () {
      // this.r.setStyle(this.el.nativeElement, 'background', 'skyblue'); // - for for easy transfer of parameters
      this.r.setStyle(this.el.nativeElement, 'color', this.color);
      this.r.setStyle(this.el.nativeElement, 'fontWeight', this.fontWeight);
      // /* for  dirStyles */
      this.r.setStyle(this.el.nativeElement, 'fontWeight', this.dirStyles.fontWeight);
      this.r.setStyle(this.el.nativeElement, 'border', this.dirStyles.border);
      this.r.setStyle(this.el.nativeElement, 'borderRadius', this.dirStyles.borderRadius);

    }

    @HostListener('mouseleave') onLeave() {
      // this.r.setStyle(this.el.nativeElement, 'background', null);
      this.r.setStyle(this.el.nativeElement, 'color', null );
      this.r.setStyle(this.el.nativeElement, 'fontWeight', null);
      /* for  dirStyles */
      this.r.setStyle(this.el.nativeElement, 'fontWeight', null);
      this.r.setStyle(this.el.nativeElement, 'border', null);
      this.r.setStyle(this.el.nativeElement, 'borderRadius', null);
    }
          /* for HostBinding */
    @HostListener('mousedown') onPress() {
      this.elBackColor = this.backgroundColor;
    }
    @HostListener('mouseup') onPressOut() {
      this.elBackColor = null;
    }


} 