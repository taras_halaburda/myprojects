import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appIfnot]'
})
export class InfoDirective {
  /* inject two essences */
  /* 1 TemplateRef - contains the content of the future element
  2 ViewContainerRef - contains wrapper - ng-template for the future element */
  
  /* further we accept as parameter some property which we will make set*/
  /*  ifNot ( )- method where we take the parameter that we pass in the structural directive*/
  @Input('appIfnot') set ifNot(contition: boolean) {
    if (!contition) {
      // Show element
      this.viewContainer.createEmbeddedView(this.templateRef)
    } else {
      // Hide element
      this.viewContainer.clear();
    }
  }

  constructor(private templateRef: TemplateRef<any>,
              private viewContainer: ViewContainerRef ) { }

}
