import { HttpClient, HttpEventType, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, delay, map, tap } from "rxjs/operators";

export interface Todo {
  completed: boolean,
  title: string,
  id?: number
}

@Injectable({ providedIn: 'root' })
export class TodosService {
  constructor(private http: HttpClient) { }

  addTodo(todo: Todo): Observable<Todo> {

    const headers = new HttpHeaders({
      'MyCustomHeader': Math.random().toString()
    })

    // to create a new Todo
    return this.http.post<Todo>('https://jsonplaceholder.typicode.com/todos', todo, {
      headers
    })
  }

  fetchTodos(): Observable<Todo[]> {

    let params = new HttpParams()
    params = params.append('_limit', '4')
    params = params.append('Custom', 'Something**')
    //we add to the get method the type of data with which we will work, 
    // response will be of the same format

    return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos?', {
      // params: new HttpParams().set('_limit', '3')
      params: params,
      observe: 'response'
    })

      // for error = todos9 
      /* return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos9?_limit=2') */

      // add delay for simulation real server via pipe

      .pipe(
        map(response => {
          /* console.log('Response', response); */
          return response.body
        }),
        delay(1000),
        catchError(error => {
          console.log('Error from rxjs: ', error.message)
          return throwError(error)
        })

      )
  }

  removeTodo(id: number): Observable<any> {
    return this.http.delete<void>(`https://jsonplaceholder.typicode.com/todos/${id}`, {
      observe: 'events' // - action that occurs with an asynchronous request to the server
    }).pipe(
      tap(event => {
        /* console.log(event); */
        if (event.type === HttpEventType.Sent) {
          console.log('* request = Sent', event);
        }
        if (event.type === HttpEventType.Response) {
          console.log('* Response', event);
        }
      })
    )
  }

  completeTodo(id: number): Observable<any> {
    return this.http.put<Todo>(`https://jsonplaceholder.typicode.com/todos/${id}`, {
      completed: true
    }, {
      responseType: 'json'
    })
  }

}