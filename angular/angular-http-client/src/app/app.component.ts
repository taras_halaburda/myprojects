import { Component, OnInit } from '@angular/core';
import { delay } from 'rxjs/operators';
import { Todo, TodosService } from './todos.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  todos: Todo[] = [];

  loading = false;

  todoTitle = '';

  error = '';

  constructor(private todosService: TodosService) { }

  ngOnInit(): void {


    // this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos?_limit=2')
    //   .subscribe(response => {
    //     console.log('Response : ', response);
    //     this.todos = response
    //   })

    this.fetchTodos()
  }

  addTodo() {
    // check if the input field is empty
    if (!this.todoTitle.trim()) {
      return
    }

    this.todosService.addTodo({
      title: this.todoTitle,
      completed: false
    }).subscribe(todo => {
      console.log('todo', todo);
      this.todos.push(todo)
      this.todoTitle = ''
    })

  }

  fetchTodos() {
    // add loader
    this.loading = true
    this.todosService.fetchTodos()
      .subscribe(response => {
        this.todos = response
        this.loading = false
      }, error => {
        console.log('Our error: ', error.message);
        this.error = error.message

      })
  }

  removeTodo(id: number) {
    this.todosService.removeTodo(id)
      .subscribe(resp => {
        /* console.log(resp); */
        this.todos = this.todos.filter(t => t.id !== id)
      })
  }

  completeTodo(id: number) {
    this.todosService.completeTodo(id).subscribe(todo => {
      console.log(todo);
      // if responseType: 'text' = need to do - JSON.parse -
      /* todo = JSON.parse(todo)
      console.log(todo); */
      this.todos.find(t => t.id === todo.id).completed = true
    })
  }
}
