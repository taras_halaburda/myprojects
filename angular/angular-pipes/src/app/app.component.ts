import { Component, OnInit } from '@angular/core';
import { Data } from '@angular/router';
import { Observable } from 'rxjs';

export interface Post {
  title: string
  text: string
}
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angular-pipes';

  e: number = Math.E;
  str = 'hello world';
  /* date */
  date: Date = new Date();

 /* ======================================================================================== */

  float = 0.42;

  /* Debuging */
  obj = {
    a: 1,
    b: {
      c: 2,
      d: {
        e: 3,
        f:4
      }
    }
  }
  /* ======================================================================================== */

  /* Filtering lists */

  search = '';
  searchField = 'title';

  posts: Post[] = [
    {title: 'Nori', text: 'grape silver beet broccoli kombu'},
    {title: 'Bunya ', text: 'nuts black-eyed pea prairie turnip'},
    {title: 'Sportacus', text: 'chestnut eggplant winter purslane fennel azuki bean'},
    {title: 'Voki ', text: 'orkut reddit meebo skype vimeo jajah spock an'},
    {title: 'Convore ', text: 'bebo rovio vimeo zanga handangoan'},
    {title: 'Voki ', text: 'flogging bilge rat main sheet bilge water nippe'},

  ]
  /* add post method */
  addPost() {
    this.posts.unshift({
      title: 'some title',
      text: 'some text for post'
    }) 
  }

  /* ======================================================================================== */

  /* AsyncPipe */

  promise: Promise<string> = new Promise<string>(resolve => {
    setTimeout(() => {
      resolve('Promise Resolved - its good')
    }, 4000);
  })
  /* Observable */
  // create Observable and in constructor of this class we pass object - obs
  dateNext: Observable<Date> = new Observable(obs => {
    setInterval( () => {
      obs.next(new Date())
    }, 1000)
  })
/* ======================================================================================== */

  /* Realization Observable without pipe async  */
  // $ - indicates the presence of a stream

 dateNext$: Observable<Date> = new Observable(obs => {
    setInterval( () => {
      obs.next(new Date())
    }, 1000)
  }) 

  dateNew: Data

  ngOnInit(): void {
    this.dateNext$.subscribe(date => {
      this.dateNew = date
    })
  }
}
