import { Pipe, PipeTransform } from '@angular/core';
import { Post } from '../app.component'; // it's necessary for Post[] 

@Pipe({
  name: 'filter',
  pure: false // In this case, the pipe is invoked on each change-detection cycle, even if the arguments have not changed.
})
export class FilterPipe implements PipeTransform {
/* add argument field which = searchField */
  transform(posts: Post[], search: string = '', field: string = 'title'): Post[] {
    if (!search.trim()) {
      return posts;
    }
    return posts.filter(post => {
      /*[field] - dynamic key which can be = field: string = 'title' OR  field: string = 'text' */
      return post[field].toLowerCase().includes(search.toLowerCase()); 
    })
  }

}
