import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MyValidators } from './my.validators';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],

})
export class AppComponent implements OnInit {

  form: FormGroup;
  appState = "on"

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl('', [
        Validators.email,
        Validators.required,
        MyValidators.restrictedEmails
      ], [MyValidators.uniqEmail]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(6)
      ]),
      address: new FormGroup({
        country: new FormControl('nz'),
        city: new FormControl('', Validators.required)
      }),
      skills: new FormArray([])
    })
  }

  submit() {
    if (this.form.valid) {
      console.log('Form:', this.form);
      const formData = { ...this.form.value }
      console.log('Form Data:', formData);

      this.form.reset();
    }
  }
  setCapital() {
    const cityMap = {
      nz: 'Wellington',
      ua: 'Kyiv',
      pt: 'Lisbon',

    }
    // get access to the variable
    const cityKey = this.form.get('address').get('country').value
    const city = cityMap[cityKey];
    // console.log(city);

    // for dynamic change of the field with the capital when choosing the appropriate country
    this.form.patchValue({
      address: { city: city }
    });
  }

  addSkill() {
    const control = new FormControl('', Validators.required);
    // firs method
    // (<FormArray>this.form.get('skills')).push()

    // seecond method
    (this.form.get('skills') as FormArray).push(control)
  }

  handleChange() {
    console.log(this.appState);
  }
}
