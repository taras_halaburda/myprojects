import { HttpClientModule } from "@angular/common/http";
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from "@angular/core/testing";
import { of } from "rxjs";
import { PostsComponent } from "./posts.component";
import { PostsService } from "./posts.service";

describe('PostsComponent', () => {
  let fixture: ComponentFixture<PostsComponent>
  let component: PostsComponent
  let service: PostsService

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PostsComponent],
      providers: [PostsService],
      imports: [HttpClientModule]
    })

    fixture = TestBed.createComponent(PostsComponent)
    component = fixture.componentInstance
    /* receiving instance of service */

    // 1 method - service = fixture.debugElement.injector.get(PostsService)
    // 2 method - @deprecated — from v9.0.0 use TestBed.inject instead of TestBed.get 
    service = TestBed.inject(PostsService)
  })
  // x - removes from the list this it for testing
  // xit('should fetch posts on ngOnInit', () => {
  //   const posts = [1, 2, 3]
  //   spyOn(service, 'fetch').and.returnValue(of(posts))

  //   // run a change check for the angular to update the entire state
  //   fixture.detectChanges()
  //   expect(component.posts).toEqual(posts)
  // })

  /*   it('should fetch posts on ngOnInit (promise)', waitForAsync(() => {
      const posts = [1, 2, 3]
      spyOn(service, 'fetchPromise').and.returnValue(Promise.resolve(posts))
  
      // run a change check for the angular to update the entire state
      fixture.detectChanges()
  
      fixture.whenStable().then(() => {
        expect(component.posts.length).toBe(posts.length)
        console.log('EXPECT CALLED');
      })
    })) */

  // another way via 
  it('should fetch posts on ngOnInit (promise)', fakeAsync(() => {
    const posts = [1, 2, 3]
    spyOn(service, 'fetchPromise').and.returnValue(Promise.resolve(posts))

    // run a change check for the angular to update the entire state
    fixture.detectChanges()

    tick()
    expect(component.posts.length).toBe(posts.length)

    // fixture.whenStable().then(() => {
    //   expect(component.posts.length).toBe(posts.length)
    //   console.log('EXPECT CALLED');
    // })
  }))
})