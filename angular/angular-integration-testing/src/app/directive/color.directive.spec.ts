import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { ColorDirective } from './color.directive';

@Component({
  template: `
  <p appColor="yellow">Text 1</p>
  <p appColor>Text 2</p>
  `
})
class HostComponent { }

describe('ColorDirective', () => {
  let fixture: ComponentFixture<HostComponent>

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ColorDirective, HostComponent]
    })
    fixture = TestBed.createComponent(HostComponent)
    fixture.detectChanges() // for apply changes for color
  })

  it('should create an instance', () => {
    const directive = new ColorDirective(null);
    expect(directive).toBeTruthy();
  });

  /* --------- */

  it('should apply input color', () => {
    let de = fixture.debugElement.queryAll(By.css('p'))[0]

    expect(de.nativeElement.style.backgroundColor).toBe('yellow')
  })
  it('should apply default color', () => {
    let de = fixture.debugElement.queryAll(By.css('p'))[1]
    // for change default color in directibe we must apply a universal solution 
    let directive = de.injector.get(ColorDirective)

    // simple solution
    // expect(de.nativeElement.style.backgroundColor).toBe('blue')

    // universal solution via dynamically value
    expect(de.nativeElement.style.backgroundColor).toBe(directive.defaultColor)
  })
});
