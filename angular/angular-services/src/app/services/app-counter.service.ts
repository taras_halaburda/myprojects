import { Injectable } from '@angular/core';
import { LogService } from './log.service';

// Decorator that marks a class as available to be provided and injected as a dependency.
@Injectable({
    providedIn: 'root'
}) 
export class AppCounterService {
    counter = 0 

    constructor(private logService: LogService) {

    }

    increase() {
        this.logService.log('increase counter ... ')
        this.counter++
    }

    decrease() {
        this.logService.log('decrease counter ... ')
        this.counter--
    }
}