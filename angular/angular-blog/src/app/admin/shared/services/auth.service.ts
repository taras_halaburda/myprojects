import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { FbAuthResponse, User } from "src/app/shared/interfaces";
import { Observable, Subject, throwError } from "rxjs";
import { environment } from "src/environments/environment";
import { catchError, tap } from "rxjs/operators";

@Injectable({ providedIn: 'root' }) // registration AuthService at the global level

export class AuthService {
  // stream
  public error$: Subject<string> = new Subject<string>()

  constructor(private http: HttpClient) { }

  get token(): string {
    //check the token by date
    const expDate = new Date(localStorage.getItem('fb-token-exp'))
    if (new Date() > expDate) {
      // clear logic token via logout() method
      this.logout()
      return null
    }
    return localStorage.getItem('fb-token')
  }

  login(user: User): Observable<any> {
    user.returnSecureToken = true
    return this.http.post(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.apiKey}`, user)
      .pipe(
        tap(this.setToken),
        // bind(this) - so that there is no loss of context
        catchError(this.handleError.bind(this))
      )
  }
  logout() {
    this.setToken(null)
  }
  // authentication check
  isAuthenticated(): boolean {
    return !!this.token
  }

  private handleError(error: HttpErrorResponse) {
    const { message } = error.error.error;
    console.log('error message >>', message);

    switch (message) {
      case 'INVALID_EMAIL':
        // for show new message of error
        this.error$.next('Wrong email')
        break;
      case 'INVALID_PASSWORD':
        this.error$.next('Incorrect password')
        break;
      case 'EMAIL_NOT_FOUND':
        this.error$.next('Email not found')
        break;
    }

    return throwError(error)
  }

  private setToken(responce: FbAuthResponse | null) {
    if (responce) {
      // logig for change token
      console.log('setToken responce', responce);
      // convert time (3600 sec) in milliseconds
      const expDate = new Date(new Date().getTime() + +responce.expiresIn * 1000)
      localStorage.setItem('fb-token', responce.idToken)
      localStorage.setItem('fb-token-exp', expDate.toISOString())
    } else {
      localStorage.clear();
    }


  }
}
